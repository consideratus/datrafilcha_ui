build_container:
	@echo "building application container"
	cp requirements.txt dockerfiles/ || exit 1
	podman build -f dockerfiles/Dockerfile_uwsgi -t app dockerfiles/

.ONSHELL:
create_build_dir:
	@echo "Check build dir"
	@if ! [ -d build ] ; then mkdir app ; fi
	@if ! [ -d build/static ] ; then mkdir app/static ; fi
	@if ! [ -d build/dynamic ] ; then mkdir app/dynamic ; fi

build: create_build_dir
	@echo "building Django app"
	rsync -avz datrafilcha_ui build/dynamic/

deploy_local:
	@echo "Local deploy"
	ansible-playbook deploy/local_setup.yml -i deploy/inventory.ini

destroy_local:
	@echo "removing containers"
	podman rm -f nginx uwsgi postgresql
