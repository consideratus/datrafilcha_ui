from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('datasets/<int:pk>/update/', views.DataSetDescriptionUpdate.as_view(), name="datasetdescriptionupdate"),
    path('deletedatasets/<int:pk>/', views.DataSetDelete.as_view(), name="datasetdelete"),
    path('graphs/<int:pk>/', views.GraphView.as_view(), name='viewgraph'),
    path('graphs/<int:pk>/update/', views.GraphDescriptionUpdate.as_view(), name='graphdescriptionupdate'),
    path('graph/', views.graph, name='graph'),
    path('createnewchart/<int:source_data_id>/<str:column_name>/', views.create_new_chart, name='createnewchart'),
    path('listofgraphs/', views.list_graphs, name='listofgraphs'),
    path('listofdatasets/', views.ListDataSets.as_view(), name='datasets'),
    path('sampledata/<int:pk>/', views.sample_data, name='sampledata'),
    path('uploaddataset/', views.UploadDataSet.as_view(), name='uploaddataset'),
    ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
