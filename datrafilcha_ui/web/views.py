import logging

from datrafilcha.facade import DataFiltersFacade

from django import forms
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic.edit import View, UpdateView, CreateView

from .forms import PageForm, FrequencyOfOccurrencesCfgForm, UploadFileForm
from .forms import GetUniqValuesCfgForm, DataSetsDescriptionForm

from .models import DataSets, GeneratedGraph, DescriptionOfGraph

from .modules.sampler import _create_sample_data
from .modules.support import _chart_maybe_exists, _create_chart_record
from .modules.support import _store_chart, _select_filter_transformer


class DataSetDelete(View):
    def post(self, request, **kwargs):
        data_set_id = kwargs['pk']
        data_set = DataSets.objects.get(pk=data_set_id)
        data_set.delete()

        return render(request, 'web/index.html')


class DataSetDescriptionUpdate(UpdateView):
    model = DataSets
    fields = ['description', 'title']

    def get(self, request, **kwargs):
        data_set_id = kwargs['pk']
        data_description = DataSets.objects.get(pk=data_set_id)

        form = DataSetsDescriptionForm(instance=data_description)

        return render(request, 'web/datasetdescription_update.html', {'form': form, 'data_set_id': data_set_id})


class GraphDescriptionUpdate(UpdateView):
    model = DescriptionOfGraph
    fields = ['description']

    def get(self, request, **kwargs):

        graph_id = kwargs['pk']
        db_manager_gg = GeneratedGraph.objects
        db_manager_gd = DescriptionOfGraph.objects

        generated_graph_id = db_manager_gg.all().values().get(pk=graph_id)['description_id']
        graph_description = db_manager_gd.all().values().get(pk=generated_graph_id)

        page_form = PageForm(graph_description)

        content = {
            'form': page_form,
            'graph_id': graph_id,
        }

        return render(request, 'web/graphdescription_update.html', content)

    def post(self, request, **kwargs):
        graph_id = kwargs['pk']

        parent_graph = GeneratedGraph.objects.all().values().get(pk=graph_id)['description_id']
        parent_graph_description = DescriptionOfGraph.objects.get(pk=parent_graph)

        page_form = PageForm(request.POST, instance=parent_graph_description)

        page_form.save()

        # Ugly and hardcoded
        url = '{}/{}'.format("/web/graphs", graph_id)
        return redirect(url)


class GraphView(View):
    def get(self, request, **kwargs):

        graph_id = kwargs['pk']

        db_manager_gg = GeneratedGraph.objects
        db_manager_gd = DescriptionOfGraph.objects

        graph_name = db_manager_gg.all().values().get(pk=graph_id)['graph_name']

        path = '/'.join(['/tmp', str(graph_name)])
        with open(path, 'r') as cache:
            html_plot = cache.read()

        graph_description_id = db_manager_gg.all().values().get(pk=graph_id)['description_id']
        graph_description = db_manager_gd.all().values().get(pk=graph_description_id)['description']

        content = {
            'graph': html_plot,
            'graph_id': graph_id,
            'description': graph_description,
        }

        return render(request, 'web/template.html', content)

    def post(self, request, **kwargs):
        print("deleting")
        graph_id = kwargs['pk']
        db_manager_gg = GeneratedGraph.objects.get(pk=graph_id)
        db_manager_gg.delete()

        return render(request, 'web/index.html')


def create_new_chart(request, **kwargs):
    """
    This is primarily called by sample_data()
    """

    # We operate on concrete data file, that must be provided
    data_id = kwargs['source_data_id']
    column_name = kwargs['column_name']

    # Set default configuration values for forms
    guv_cfg = {
        'cfg_id': 'UniqValues',
        'cfg_column_name': column_name,
        'raw_data_id': data_id,
        'cfg_desc': True,
        'cfg_limit': 5,
        'cfg_value': "Uniq elements"
    }

    foo_cfg = {
        'cfg_id': 'FrequencyOfOccurrences',
        'cfg_column_name': column_name,
        'raw_data_id': data_id,
        'cfg_limit': 5,
        'cfg_value': "value",
    }

    # Pre-fill forms - validate forms..
    guv_form = GetUniqValuesCfgForm(guv_cfg)
    foo_form = FrequencyOfOccurrencesCfgForm(foo_cfg)

    # Create context dictionary, key is name for table, value is rendered html form
    forms = {
        'Get uniq values': guv_form,
        'Get Frequency of occurrences': foo_form,
    }

    return render(request, 'web/graph_user_data.html', {'form': forms})


# cache + pre-cache all existing graphs.
def graph(request):
    """
    View for data processing
    """

    form = _select_filter_transformer(request)
    if not form:
        # Some error page
        return redirect("/web/")

    raw_data_id = form.cleaned_data['raw_data_id']

    data_source = get_object_or_404(DataSets, id=str(raw_data_id))
    data_source_file_name = data_source.title

    # Create configuration dictionary for filter, strip cfg prefix from keys
    filter_configuration = {k.removeprefix('cfg_'): v 
                            for k, v in form.cleaned_data.items()
                            if 'cfg_' in k}

    column = form.cleaned_data['cfg_column_name']
    chart_id, chart_hash = _chart_maybe_exists(filter_configuration,
                                               data_source_file_name, column)
    if chart_id:
        existing_chart = '{}/{}'.format("/web/graphs", chart_id)
        return redirect(existing_chart)

    # This should be interfaced
    data_filters_facade = DataFiltersFacade()
    data_source_file = '{}/{}'.format('/tmp', str(data_source.file))

    # This should run asynchronously
    html_plot = data_filters_facade.csv_column(filter_configuration,
                                               data_source_file)

    error = _store_chart(chart_hash, html_plot)
    if error:
        print("Error saving chart")
        print(error)

    # Save record of newly created chart.
    chart_record_id, chart_description = _create_chart_record(chart_hash,
                                                              data_source)

    content = {
        'graph': html_plot,
        'graph_id': chart_record_id,
        'description': chart_description,
    }

    return render(request, 'web/template.html', content)


def index(request):
    return render(request, 'web/index.html')


class ListDataSets(View):
    model = DataSets
    fields = ['title', 'original_source_url', 'file']

    def get(self, request):

        data = self.model.objects.all().values()

        return render(request, 'web/datasets_form.html', {'data': data})


def list_graphs(request):
    graphs_list = list()
    graphs_list = GeneratedGraph.objects.all()
    context = {
        'graphs_list': graphs_list,
    }

    return render(request, 'web/list_of_graphs.html', context)


# cache
def sample_data(request, **kwargs):
    """
    creates sample data from any file.

    Usually called by ListDataSets()
    """
    print("sampling")
    # Get file from storage
    pk = str(kwargs['pk'])

    try:
        data_set = DataSets.objects.all().values().get(pk=pk)
    except ObjectDoesNotExist:
        print("Cannot find database record")
        return HttpResponseRedirect('/web/listofdatasets')

    file = data_set['file']

    # Hardcoded
    path_to_file = '{}/{}'.format('/tmp', file)
    sample_data = _create_sample_data(path_to_file)
    context = {
        'metadata': data_set,
        'data_source_id': pk,
        'sample_data': sample_data,
    }

    return render(request, 'web/sample_data.html', context)


class UploadDataSet(CreateView):
    model = DataSets
    template_name_suffix = '_upload'
    fields = ['file', 'title', 'description', 'original_source_url']

    def post(self, request):
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('datasets')

        return HttpResponse("{}\n{}".format("Form is not valid", forms.errors))
