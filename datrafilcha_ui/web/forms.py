from django import forms
from web.models import DataSets, DescriptionOfGraph


# Graph forms
class BarChartForm(forms.Form):
    data_id = forms.IntegerField(widget=forms.HiddenInput, initial=True, required=True)
    cfg_id = forms.CharField(widget=forms.HiddenInput, initial=True)
    cfg_chart_title = forms.CharField(label='Title', max_length=300, required=False)


class LineChartForm(forms.Form):
    data_id = forms.CharField(widget=forms.HiddenInput, initial=True, required=True)
    cfg_id = forms.CharField(widget=forms.HiddenInput, initial=True)
    cfg_chart_title = forms.CharField(label='Title', max_length=300, required=False)


# Data filter forms
class GetUniqValuesCfgForm(forms.Form):
    cfg_column_name = forms.CharField(widget=forms.HiddenInput, label='Column name', max_length=100, required=True)
    raw_data_id = forms.IntegerField(widget=forms.HiddenInput, required=True)
    cfg_id = forms.CharField(widget=forms.HiddenInput, initial=True)
    cfg_desc = forms.BooleanField(label='Description label')
    cfg_limit = forms.IntegerField(label='Limit', min_value=1, max_value=99)
    cfg_value = forms.CharField(widget=forms.HiddenInput, initial=True, required=True)


class FrequencyOfOccurrencesCfgForm(forms.Form):
    cfg_column_name = forms.CharField(widget=forms.HiddenInput, label='Column name', max_length=100, required=True)
    raw_data_id = forms.IntegerField(widget=forms.HiddenInput, required=True)
    cfg_id = forms.CharField(widget=forms.HiddenInput, initial=True)
    cfg_value = forms.CharField(label='Value', max_length=100, required=True)


# Helper forms
class DataSetsDescriptionForm(forms.ModelForm):
    class Meta:
        model = DataSets
        fields = ['description', 'title']


class DisplayFileForm(forms.ModelForm):
    class Meta:
        model = DataSets
        fields = ['description', 'file', 'original_source_url', 'title']
        widgets = {
            'file': forms.HiddenInput,
        }


class PageForm(forms.ModelForm):
    class Meta:
        model = DescriptionOfGraph
        fields = ['description', 'title']


class UploadFileForm(forms.ModelForm):
    class Meta:
        model = DataSets
        fields = ['description', 'file', 'original_source_url', 'title']
