from django.db import models
from django.urls import reverse


class DataSets(models.Model):
    description = models.TextField(max_length=100000, default="some description")
    file = models.FileField(upload_to='web_datafiles', default='settings.MEDIA_ROOT/noname.data')
    original_source_url = models.URLField(max_length=300, default="https://google.com")
    title = models.CharField(max_length=300, default="User-friendly title")

    def get_absolute_url(self):
        return reverse('viewgraph', kwargs={'pk': self.pk})

    def __str__(self) -> str:
        return '%s %s %s' % (self.file, self.description, self.original_source_url)


class DescriptionOfGraph(models.Model):
    description = models.TextField(max_length=100000, default="some description")
    title = models.CharField(max_length=300, default="User-friendly title")

    def get_absolute_url(self):
        return reverse('viewgraph', kwargs={'pk': self.pk})


class GeneratedGraph(models.Model):
    data_source = models.ForeignKey(DataSets, on_delete=models.CASCADE)
    description = models.ForeignKey(DescriptionOfGraph, on_delete=models.CASCADE)
    charting_function = models.CharField(max_length=256, default="none")
    graph_name = models.CharField(max_length=256, default="none")

    def get_absolute_url(self):
        return reverse('viewgraph', kwargs={'pk': self.pk})
