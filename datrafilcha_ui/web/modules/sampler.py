
import csv


def _create_sample_data(path_to_file: str) -> list[dict]:

    print("Sample helper package")

    sample_list = list()
    with open(path_to_file) as csvfile:
        print("sampling CSV file")
        csv_reader = csv.DictReader(csvfile, delimiter=',')

        for i, row in enumerate(csv_reader):
            print("iterating...")
            if i > 10:
                break
            sample_list.append(row)

    print("here")

    return sample_list
