
from django.http import HttpResponse
from ..forms import FrequencyOfOccurrencesCfgForm, GetUniqValuesCfgForm
from ..models import GeneratedGraph, DescriptionOfGraph


def _chart_maybe_exists(configuration, data_source_file_name, column):
    # Prepare the conf values for hashing
    for k, v in configuration.items():
        uniq_view = '+'.join([str(k), str(v)])
    uniq_view = '+'.join([data_source_file_name, column])
    uniq_view_bytes = bytes(uniq_view, 'utf-8')

    # In class based view this should be in constructor
    import hashlib
    hash = hashlib.sha256()
    hash.update(uniq_view_bytes)
    chart_hash = hash.hexdigest()

    try:
        graph = GeneratedGraph.objects.get(graph_name=chart_hash)
        print("Graph exists already, redirect")
    except GeneratedGraph.DoesNotExist:
        print("Creating new graph")
        return (None, chart_hash)
    else:
        return (graph.pk, chart_hash)


def _create_chart_record(chart_hash, data_source):
    chart_description = DescriptionOfGraph(description="TBD")
    chart_description.save()
    chart = GeneratedGraph(graph_name=chart_hash,
                               charting_function="TBD",
                               data_source=data_source,
                               description=chart_description)
    chart.save()
    chart_record_id = chart.pk

    return (chart_record_id, chart_description)


def _select_filter_transformer(request):

    if not request.method == 'POST':
        return HttpResponse('Invalid request: ' + request.method,  status=400)

    if str(request.POST['cfg_id']) == 'UniqValues':
        try:
            guv_form = GetUniqValuesCfgForm(request.POST)
        except Exception as e:
            print(e)

        if guv_form.is_valid():
            form = guv_form
            print("is valid")
        else:
            print("is not valid")

    if str(request.POST['cfg_id']) == 'FrequencyOfOccurrences':
        try:
            foo_form = FrequencyOfOccurrencesCfgForm(request.POST)
        except Exception as e:
            print(e)

        if foo_form.is_valid():
            print("is valid")
            form = foo_form
        else:
            print("foo form is not valid")

    if not form:
        return None

    return form


def _store_chart(chart_name, html_data):
    # Save graph to FSDB
    path = '/'.join(['/tmp', str(chart_name)])
    try:
        with open(path, 'w') as file:
            file.write(html_data)
        print("Html plot saved: " + str(path))
    except Exception as e:
        return e
    else:
        return None
